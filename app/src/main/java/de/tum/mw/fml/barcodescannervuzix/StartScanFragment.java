package de.tum.mw.fml.barcodescannervuzix;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class StartScanFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private final String TAG = "StartScanFragment";
    public StartScanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_start_scan, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void scan() {
        Intent scanIntent = new Intent(getString(R.string.scan_action));
        Log.i(TAG, "starting activity CaptureActivity");
        //start activity using parent activity.Otherwise requestcode will be changed internally.
        getActivity().startActivityForResult(scanIntent, Integer.parseInt(getString(R.string.scan_request_code)));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
