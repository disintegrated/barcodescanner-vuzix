package com.google.zxing.client.glass;

import com.vuzix.hardware.VuzixCamera;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.zxing.client.android.camera.CameraConfigurationUtils;

public final class CameraConfigurationManager{

    private static final String TAG = "CameraConfiguration";

    public static final int ZOOM = 1;
    private Context mContext;
    private static CameraConfigurationManager _instance;

    public Point bestPreviewSize ;

    private CameraConfigurationManager() {}

    public static CameraConfigurationManager Instance(Context context)
    {
        if(null ==_instance)
        {
            _instance = new CameraConfigurationManager();
            _instance.mContext = context;
            _instance.bestPreviewSize = null;
        }

        return _instance;
    }

    public void configure(VuzixCamera camera) {
        VuzixCamera.Parameters parameters = camera.getParameters();

        setPreviewSize(parameters, camera);
        setFocusMode(parameters);
        //parameters.setPictureSize(1920, 1080); //this is the best value with the correct orientation
        configureAdvanced(parameters);
        camera.setParameters(parameters);
        //logAllParameters(parameters);
        Log.i(TAG, "configure: preview format " + parameters.getPictureFormat()
        ); ;
    }

    private void setFocusMode(VuzixCamera.Parameters parameters)
    {
        parameters.setFocusMode(VuzixCamera.Parameters.FOCUS_MODE_INFINITY);
    }
    private void configureAdvanced(VuzixCamera.Parameters parameters) {
        CameraConfigurationUtils.setBestPreviewFPS(parameters);
        CameraConfigurationUtils.setBarcodeSceneMode(parameters);
        CameraConfigurationUtils.setVideoStabilization(parameters);
        CameraConfigurationUtils.setMetering(parameters);
        //decide if setExposureCompensation needs to be called
        CameraConfigurationUtils.setZoom(parameters, ZOOM);
    }

    private void logAllParameters(VuzixCamera.Parameters parameters) {
        if (Log.isLoggable(TAG, Log.INFO)) {
            for (String line : CameraConfigurationUtils.collectStats(parameters).split("\n")) {
                Log.i(TAG, line);
            }
        }
    }

    private void setPreviewSize(VuzixCamera.Parameters parameters, VuzixCamera camera)
    {
        if(null == _instance.bestPreviewSize)
        {
            WindowManager manager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();

            Point theScreenResolution = new Point();
            display.getSize(theScreenResolution);

            //bestPreviewSize = CameraConfigurationUtils.findBestPreviewSizeValue(parameters, theScreenResolution);
            bestPreviewSize = new Point(1920,1080);
            Log.d(TAG, "setPreviewSize: best preview size "+ bestPreviewSize.x + " "+bestPreviewSize.y);
        }

        parameters.setPreviewSize(bestPreviewSize.x, bestPreviewSize.y);

        camera.setParameters(parameters);
    }
}