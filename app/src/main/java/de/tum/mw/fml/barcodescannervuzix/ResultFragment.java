package de.tum.mw.fml.barcodescannervuzix;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ResultFragment extends Fragment {

    private final String TAG = "ResultFragment";
    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if(args != null)
        {
            Activity activity = getActivity();
            TextView status = (TextView) activity.findViewById(R.id.status_view);
            status.setText(args.getString(activity.getString(R.string.scan_status_key)));
            TextView result = (TextView) activity.findViewById(R.id.result);
            result.setText(args.getString(activity.getString(R.string.scan_result_key)));
            TextView duration = (TextView) activity.findViewById(R.id.duration);
            duration.setText(args.getDouble(activity.getString(R.string.scan_duration_key)) + " sec");
        }
        else
        {
            Log.d(TAG, "onStart: No bundle argument was sent");
        }
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
