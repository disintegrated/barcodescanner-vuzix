package de.tum.mw.fml.barcodescannervuzix;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.FragmentActivity;

import com.google.zxing.Result;

public class MainActivity extends FragmentActivity {

    private String scannedResult;
    private final String TAG_FRAGMENT = "TAG_FRAGMENT";
    private final String TAG = "MainActivity";
    private ResultFragment resultFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ResultFragment resultFragment = null;
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.result_frame) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            InitialFragment intialFragment = new InitialFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            intialFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.result_frame, intialFragment).commit();
        }
        if (findViewById(R.id.scan_button_frame) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            StartScanFragment startScanFragment = new StartScanFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            startScanFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.scan_button_frame, startScanFragment, TAG_FRAGMENT).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d(TAG, "onActivityResult: ");
        if(requestCode == Integer.parseInt(getString(R.string.scan_request_code)))
        {
            resultFragment = new ResultFragment();
            resultFragment.setArguments(data.getExtras());
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Log.d(TAG, "onKeyDown");
        if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)
        {
            Log.d(TAG, "onKeyDown: Key code 'Enter' detected");
            StartScanFragment scanFragment = (StartScanFragment)getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
            scanFragment.scan();

            return  true;
        }
        else if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN)
        {
            Log.d(TAG, "onKeyDown: LongPress detected. Existing application");
            finish();
        }
        return false;
    }

    @Override
    public void onResumeFragments()
    {
        Log.d(TAG, "onResumeFragments: ");
        if(null != resultFragment)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.result_frame, resultFragment).commit();
        }
    }
}
