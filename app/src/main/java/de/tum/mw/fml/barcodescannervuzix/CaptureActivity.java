package de.tum.mw.fml.barcodescannervuzix;

/**
 * Created by RabbidDog on 1/17/2016.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ParsedResultType;
import com.google.zxing.client.result.ResultParser;
import com.google.zxing.client.result.TextParsedResult;
import com.google.zxing.client.result.URIParsedResult;
import com.vuzix.hardware.VuzixCamera;

import java.io.IOException;

import com.google.zxing.client.glass.CameraConfigurationManager;

public class CaptureActivity extends Activity implements SurfaceHolder.Callback, VuzixCamera.OnZoomChangeListener, VuzixCamera.AutoFocusCallback {

    private static final String TAG = "CaptureActivity";
    private Context mContext;
    private VuzixCamera mCamera;
    private CameraConfigurationManager mConfigurationManager;
    private Result mResult;
    private SurfaceHolder mSurfaceHolderWithCallBack;
    private boolean hasSurface;
    private boolean returnResult;
    private DecodeRunnable decodeRunnable;

    private long startTime;
    private long endTime;

    /*from surfaceholder callback*/
    @Override
    public synchronized void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "surfaceCreated: ");
        mSurfaceHolderWithCallBack = null; //reassigned in onResume()
        if (!hasSurface) {
            hasSurface = true;
            SafeCameraOpen(surfaceHolder);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        //do  nothing for now
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i(TAG, getString(R.string.surface_destroyed));
        setCamera(null, null);
        mSurfaceHolderWithCallBack = null;
        hasSurface = false;
    }

    /*from vuzix camera*/
    @Override
    public void onZoomChange(int i, boolean b, VuzixCamera vuzixCamera) {

    }

    /*Autofocus callback*/
    @Override
    public void onAutoFocus(boolean b, VuzixCamera vuzixCamera) {

    }

    /*activity methods*/
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mContext = getApplicationContext();
        mConfigurationManager = CameraConfigurationManager.Instance(mContext);
        Log.i(TAG, "On Create");
        Intent intent = getIntent();
        returnResult = intent != null && getString(R.string.scan_action).equals(intent.getAction());
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);
    }

    @Override
    public synchronized void onResume() {
        Log.i(TAG, "On Resume");
        super.onResume();
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (surfaceHolder == null) {
            throw new IllegalStateException("No Surface Holder");
        }
        if (hasSurface) {
            SafeCameraOpen(surfaceHolder);
        } else {
            surfaceHolder.addCallback(this);
            mSurfaceHolderWithCallBack = surfaceHolder;
        }
    }

    @Override
    protected void onPause() {
        mResult = null;
        if (decodeRunnable != null) {
            decodeRunnable.stop();
            decodeRunnable = null;
        }
        if (mCamera != null) {
            setCamera(null, null);
        }
        if (mSurfaceHolderWithCallBack != null) {
            mSurfaceHolderWithCallBack.removeCallback(this);
            mSurfaceHolderWithCallBack = null;
        }
        super.onPause();
    }

    private void SafeCameraOpen(SurfaceHolder holder) {
        //record time from when camera starts
        startTime = System.currentTimeMillis();
        Log.i(TAG, "SafeCameraOpen: startTime : " + startTime);
        try {
            releaseCameraAndPreview();
            VuzixCamera camera = VuzixCamera.open();
            setCamera(camera, holder);
        } catch (Exception e) {
            Log.e(TAG, getString(R.string.camera_open_failed));
            e.printStackTrace();
        }


        //start decoder in another thread
        decodeRunnable = new DecodeRunnable(this, mCamera);
        new Thread(decodeRunnable).start();
        reset();
    }

    private void releaseCameraAndPreview() {
        setCamera(null, null);
    }

    /*
    * source: http://developer.android.com/training/camera/cameradirect.html*/
    private void setCamera(VuzixCamera camera, SurfaceHolder holder) {
        if (mCamera == camera) {
            return;
        }

        stopPreviewAndFreeCamera();
        mCamera = camera;

        if (mCamera != null) {
            mConfigurationManager.configure(mCamera);

            try {
                mCamera.setPreviewDisplay(holder);
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.cannot_start_preview));
            }
            mCamera.startPreview();
        }
    }

    /*
    * source : http://developer.android.com/training/camera/cameradirect.html*/
    private void stopPreviewAndFreeCamera() {
        if (mCamera != null) {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();

            // Important: Call release() to release the camera for use by other
            // applications. Applications should release the camera immediately
            // during onPause() and re-open() it during onResume()).
            mCamera.release();

            mCamera = null;
        }
    }

    void setResult(Result result) {
        //record the end of decoding
        endTime = System.currentTimeMillis();
        Log.i(TAG, "setResult: endtime : " + endTime);
        //stop the decoder thread
        decodeRunnable.stop();
        if (returnResult) {
            Intent scanResult = new Intent(getString(R.string.scan_action));
            Bundle extras = new Bundle();

            double duration = (endTime - startTime) / 1000.0;
            if (null == result) {
                extras.putString(getString(R.string.scan_result_key), "Empty");
                Log.i(TAG, "setResult: scan result was null");
                extras.putString(getString(R.string.scan_status_key), getString(R.string.scan_failed));
                setResult(RESULT_CANCELED, scanResult);
            } else {
                extras.putString(getString(R.string.scan_result_key), result.getText());
                Log.i(TAG, "setResult: scan result was not null");
                extras.putString(getString(R.string.scan_status_key), getString(R.string.scan_succeded));
                setResult(RESULT_OK, scanResult);
            }

            extras.putDouble(getString(R.string.scan_duration_key), duration);
            scanResult.putExtras(extras);
            finish();
        } else {
            TextView statusView = (TextView) findViewById(R.id.status_view);
            if (null == result) {
                statusView.setText(getString(R.string.scan_failed));
                statusView.setVisibility(View.VISIBLE);
            } else {
                String text = result.getText();
                statusView.setText(text);
                statusView.setTextSize(TypedValue.COMPLEX_UNIT_SP, Math.max(14, 56 - text.length() / 4));
                statusView.setVisibility(View.VISIBLE);
                this.mResult = result;
            }
        }
    }

    private void handleResult(Result result) {
        ParsedResult parsed = ResultParser.parseResult(result);
        Intent intent;
        if (parsed.getType() == ParsedResultType.URI) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(((URIParsedResult) parsed).getURI()));
        } else {
            intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra("query", ((TextParsedResult) parsed).getText());
        }
        startActivity(intent);
    }

    private synchronized void reset() {
        TextView statusView = (TextView) findViewById(R.id.status_view);
        if(null != statusView)
        {
            statusView.setVisibility(View.GONE);
        }
        mResult = null;
        decodeRunnable.startScanning();
    }
}